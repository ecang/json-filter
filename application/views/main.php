
<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Title Page</title>

        <!-- Bootstrap CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
  

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.3/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
    <h1 class="text-center">Hello World</h1>
    <div class="row col-md-12">
        
        <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 col-offset-5">
            <form action="" method="POST" class="form-inline" role="form">
            
                <div class="form-group">
                    <label class="sr-only" for="">label</label>
                    
                    <select name="Umur" id="umur" class="form-control" required="required" data-url="<?=base_url('index.php/Welcome/filter')?>">
                        <option value="all">Semua</option>
                        <option value="32">32</option>
                        <option value="33">33</option>
                        <option value="34">34</option>
                        <option value="35">35</option>
                    </select>
                    
                </div>
        
            </form>
        </div>
        
        <table class="table table-bordered table-hover" id="tables">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Age</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        
        
        
        
    </div>
    
        

        <!-- jQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
        <script>
        $(document).ready(function () {
            var url = $('#umur').data('url')+'/'+$('#umur').val();
            var table = $('#tables').DataTable({
                "processing": true,
                "ajax": {
                    "url": url,
                    "type": "POST"
                }
            } );
            $('#umur').change(function (e) { 
               var nurl = $(this).data('url')+'/'+$(this).val();
            //    console.log(nurl);
               
               table.ajax.url( nurl ).load();
                
            });
            // $.getJSON(url, function (data) {
            //     $.each(data.umu, function(key, value) {   
            //         $('#umur')
            //             .append($("<option></option>")
            //                         .attr("value",key)
            //                         .text(value)); 
            //     });
            //     }
            // );

        });
        </script>
    </body>
</html>
