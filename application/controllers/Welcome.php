<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('main');
	}
	public function filter($age)
	{
		$src = file_get_contents('http://dummy.restapiexample.com/api/v1/employees');
		$someArray = json_decode($src, true);
		$output = array();
		foreach ($someArray as $value) {
			$data = array();
			if ($age !=0) {
				if ( $value['employee_age'] == $age) { 
   
					$data[] = $value['id'];
					$data[] = $value['employee_name'];
					$data[] = $value['employee_age'];
					$output[] = $data;
				}
			}else{
				$data[] = $value['id'];
					$data[] = $value['employee_name'];
					$data[] = $value['employee_age'];
					$output[] = $data;
			}
			
		}
		// echo json_encode($someArray);
		echo json_encode(array('data'=>$output));
	}
}
